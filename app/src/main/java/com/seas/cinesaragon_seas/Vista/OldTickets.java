package com.seas.cinesaragon_seas.Vista;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.seas.cinesaragon_seas.Controller.Extras.AppPreference;
import com.seas.cinesaragon_seas.Controller.Extras.Container;
import com.seas.cinesaragon_seas.R;

public class OldTickets extends AppCompatActivity {

    private TextView movietittle, cinematittle, sesiones, seats, userid;
    private String mtittle, ctittle, times, seat, picture, usermail, sesion, date;
    private ImageView mPicture, qR;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_old_ticket);

        movietittle = findViewById(R.id.movie);
        cinematittle = findViewById(R.id.cinema);
        sesiones = findViewById(R.id.sesion);
        //userid = findViewById(R.id.userid);
        seats = findViewById(R.id.date);



        Intent tickedResume = getIntent();

        //seat = tickedResume.getStringExtra("butaca");
        ctittle = tickedResume.getStringExtra("cine");
        mtittle = tickedResume.getStringExtra("pelicula");
        usermail = tickedResume.getStringExtra("usuario");
        sesion = tickedResume.getStringExtra("sesion");
        date = tickedResume.getStringExtra("date");


        movietittle.setText(mtittle);
        cinematittle.setText(ctittle);
        sesiones.setText(sesion);
        //userid.setText(usermail);
        seats.setText(date);

        Button btnBack;

        btnBack =  (Button) findViewById(R.id.btnprint);
        btnBack.setText("Volver a mi perfil");

        btnBack.setOnClickListener(new View.OnClickListener()  {
            @Override
            public void onClick(View view) {

               Intent intent = new Intent(OldTickets.this, Container.class);
               startActivity(intent);

            }

        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        //añade menú
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_movie_detail, menu);

        return true;


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_cines) {
            Intent intent = new Intent(OldTickets.this, CinemaList.class);
            intent.putExtra("email", usermail);
            startActivity(intent);
            return true;
        }

        //check login status from sharedPreference
        item.setTitle("Mi perfil");
            if (id == R.id.action_perfil) {


                Intent intent = new Intent(OldTickets.this, Container.class);
                intent.putExtra("email", usermail);

                startActivity(intent);
                return true;
            }




        return super.onOptionsItemSelected(item);
    }


}
