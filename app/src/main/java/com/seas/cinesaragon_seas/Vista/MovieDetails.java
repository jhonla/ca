package com.seas.cinesaragon_seas.Vista;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import com.seas.cinesaragon_seas.Controller.Adapter.MovieAdapter;
import com.seas.cinesaragon_seas.Controller.Adapter.VideoAdapter;
import com.seas.cinesaragon_seas.Controller.Extras.Container;
import com.seas.cinesaragon_seas.Model.Movies;
import com.seas.cinesaragon_seas.Model.Remote.API;
import com.seas.cinesaragon_seas.Model.Trailers;
import com.seas.cinesaragon_seas.R;


import java.util.List;
import java.util.Vector;


public class MovieDetails extends AppCompatActivity {

    private Spinner mGenderSpinner;
    private TextView mName, mTitle;
    private ImageView mPicture, mPictures;
    private FloatingActionButton mFabChoosePic;
    private RecyclerView recyclerView;
    Vector<Trailers> trailers = new Vector<>();
    private RecyclerView.LayoutManager layoutManager;
    private MovieAdapter adapter;
    private List<Movies> moviesList;
    API apiInterface;
    MovieAdapter.RecyclerViewClickListener listener;
    ProgressBar progressBar;
    private String name, descripcion, trailer, picture, se1, se2, se3, se4,userID;
    private int id;
    private Menu action;
    private Bitmap bitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_movie_detail);
//Se asignas los id de las etiquetas del layout de movie_details a las variables
        mName = findViewById(R.id.tv_movie_title_reserve);
        mTitle = findViewById(R.id.tv_summary);
        mPicture = findViewById(R.id.img_poster);
        mPictures = findViewById(R.id.img_reserve_movie);
        Intent userIdIntent = getIntent();
        userID = userIdIntent.getStringExtra("email");

//traemos los datos pasados de la lista de poeliculas con getInExtra
        Intent intent = getIntent();
        id = intent.getIntExtra("id", 0);
        name = intent.getStringExtra("name");
        descripcion = intent.getStringExtra("descripcion");
        picture = intent.getStringExtra("picture");
        trailer = intent.getStringExtra("trailer");
        se1 = intent.getStringExtra("se1");
        se2 = intent.getStringExtra("se2");
        se3 = intent.getStringExtra("se3");
        se4 = intent.getStringExtra("se4");
        userID = intent.getStringExtra("email");
//aplicamos los datos a las variables para asignar a la ficha de películas.
        mName.setText(name);
        mTitle.setText(descripcion);
        RequestOptions requestOptions = new RequestOptions();
        requestOptions.skipMemoryCache(true);
        requestOptions.diskCacheStrategy(DiskCacheStrategy.NONE);
        requestOptions.placeholder(R.drawable.logo);
        requestOptions.error(R.drawable.logo);

        Glide.with(MovieDetails.this)
                .load(picture)
                .apply(requestOptions)
                .into(mPicture);

        Glide.with(MovieDetails.this)
                .load(picture)
                .apply(requestOptions)
                .into(mPictures);


        recyclerView = findViewById(R.id.video_list_recycler_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        trailers.add(new Trailers("<iframe width=\"100%\" height=\"100%\" src=\"https://www.youtube.com/embed/" + trailer + "\" frameborder=\"0\" allowfullscreen></iframe>"));
        VideoAdapter videoAdapter = new VideoAdapter(trailers);
        recyclerView.setAdapter(videoAdapter);


        Button btn = findViewById(R.id.reserva);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(v.getContext(), MovieCinema.class);
                startActivityForResult(intent, 0);
                intent.putExtra("id", id);
                intent.putExtra("name", name);
                intent.putExtra("picture", picture);
                intent.putExtra("email", userID);
                startActivity(intent);

            }
        });


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_movie_detail, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_perfil) {
            Intent intent = new Intent(MovieDetails.this, Container.class);
            intent.putExtra("email", userID);
            startActivity(intent);
            return true;
        } else if (id == R.id.action_cines) {
            Intent intent = new Intent(MovieDetails.this, CinemaList.class);
            intent.putExtra("email", userID);
            startActivity(intent);
            return true;
        } else if (id == R.id.action_peliculas) {
            Intent intent = new Intent(MovieDetails.this, MovieList.class);
            intent.putExtra("email", userID);
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


}
