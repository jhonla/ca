package com.seas.cinesaragon_seas.Vista;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;


import com.seas.cinesaragon_seas.Controller.Adapter.TicketAdapter;
import com.seas.cinesaragon_seas.Controller.Extras.AppPreference;
import com.seas.cinesaragon_seas.Controller.Extras.Container;
import com.seas.cinesaragon_seas.Model.Remote.API;
import com.seas.cinesaragon_seas.Model.Remote.ApiClient;
import com.seas.cinesaragon_seas.Model.Ticket;
import com.seas.cinesaragon_seas.R;


import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TicketList extends AppCompatActivity {

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private TicketAdapter adapter;
    private List<Ticket> ticketsList;
    API apiInterface;
    TicketAdapter.RecyclerViewClickListener listener;
    ProgressBar progressBar;
    String userID;
    public static AppPreference appPreference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        apiInterface = ApiClient.getApiClient().create(API.class);
        progressBar = findViewById(R.id.progress);
        recyclerView = findViewById(R.id.recyclerView);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        Intent userIdIntent = getIntent();
        userID = userIdIntent.getStringExtra("userId");


        listener = new TicketAdapter.RecyclerViewClickListener() {
            @Override
            public void onRowClick(View view, final int position) {

                Intent intent = new Intent(TicketList.this, OldTickets.class);
                intent.putExtra("cine", ticketsList.get(position).getCine());
                intent.putExtra("pelicula", ticketsList.get(position).getPelicula());
                intent.putExtra("usuario", ticketsList.get(position).getUsuario());
                intent.putExtra("sesion", ticketsList.get(position).getSesion());
                intent.putExtra("date", ticketsList.get(position).getDate());
                startActivity(intent);
            }

        };




    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        //añade menú
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        final SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        MenuItem searchMenuItem = menu.findItem(R.id.action_search);

        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName())
        );
        searchView.setQueryHint("Buscar entrada...");
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(final String query) {

                adapter.getFilter().filter(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                adapter.getFilter().filter(newText);
                return false;
            }
        });

        searchMenuItem.getIcon().setVisible(false, false);

        return true;


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_cines) {
            Intent intent = new Intent(TicketList.this, CinemaList.class);
            startActivity(intent);
            return true;
        }
        appPreference = new AppPreference(this);
        //check login status from sharedPreference
        if (appPreference.getLoginStatus()) {


            item.setTitle("Mi perfil");
            if (id == R.id.action_perfil) {


                Intent intent = new Intent(TicketList.this, Container.class);
                startActivity(intent);
                return true;
            }

        } else {

            item.setTitle("Iniciar sesión");
            // when get false
            if (id == R.id.action_perfil) {


                Intent intent = new Intent(TicketList.this, Container.class);
                startActivity(intent);
                return true;
            }
        }

        return super.onOptionsItemSelected(item);
    }


    public void getTickets(){

        Call<List<Ticket>> call = apiInterface.getTickets(userID);
        call.enqueue(new Callback<List<Ticket>>() {
            @Override
            public void onResponse(Call<List<Ticket>> call, Response<List<Ticket>> response) {
                progressBar.setVisibility(View.GONE);
                ticketsList = response.body();
                Log.i(TicketList.class.getSimpleName(), response.body().toString());
                adapter = new TicketAdapter(ticketsList, TicketList.this, listener);
                recyclerView.setAdapter(adapter);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<List<Ticket>> call, Throwable t) {
                Toast.makeText(TicketList.this, "rp :"+
                                t.getMessage().toString(),
                        Toast.LENGTH_SHORT).show();
            }
        });
    }


    @Override
    protected void onResume() {
        super.onResume();
        getTickets();
    }
}
