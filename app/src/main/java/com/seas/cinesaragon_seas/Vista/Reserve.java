package com.seas.cinesaragon_seas.Vista;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.journeyapps.barcodescanner.BarcodeEncoder;
import com.seas.cinesaragon_seas.Controller.Extras.AppPreference;
import com.seas.cinesaragon_seas.Controller.Extras.Container;
import com.seas.cinesaragon_seas.Controller.Extras.SendMail;
import com.seas.cinesaragon_seas.Model.Reserved;
import com.seas.cinesaragon_seas.Model.Ticket;
import com.seas.cinesaragon_seas.R;
import com.seas.cinesaragon_seas.Model.Remote.API;
import com.seas.cinesaragon_seas.Model.Remote.ApiClient;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Reserve extends AppCompatActivity implements View.OnClickListener {
    private int idMovie, idCinema;
    ArrayList<Integer> butaca = new ArrayList<>(10);

    private TextView mName;
    private String se1, tittle, ctiitle, se2, se3, se4, picturem, picturec, sesionselect, idUser;
    private ImageView mPicture, cPicture;
    private RelativeLayout relativeLayout;
    private Bitmap bitmap;
    private ToggleButton s1, s2, s3, s4;
    ViewGroup layout;
    API apiInterface;
    String dirpath;
    String qrPath;
    public static AppPreference appPreference;
    private Bitmap QRBit;
    private SimpleDateFormat formateador;

    //stringpara la creación de asientos,A: RES DSPONIBLE, B: COMPRADO: R: RESERVADO
    String seats = "_AAAAAAAAAAA_/"
            + "_________________/"
            + "AA_AAAAAAAAA_AA/"
            + "AA_AAAAAAAAA_AA/"
            + "AA_AAAAAAAAA_AA/"
            + "AA_AAAAAAAAA_AA/"
            + "AA_AAAAAAAAA_AA/"
            + "AA_AAAAAAAAA_AA/"
            + "AA_AAAAAAAAA_AA/"
            + "_________________/";

    //DISTRIBUCIÓN DE ASIENTOS
    List<TextView> seatViewList = new ArrayList<>();
    int seatSize = 100;
    int seatGaping = 10;

    int STATUS_AVAILABLE = 1;
    int STATUS_BOOKED = 2;
    int STATUS_RESERVED = 3;
    String selectedIds = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout);
        layout = findViewById(R.id.layoutSeat);

        // butaca
        seats = "/" + seats;

        LinearLayout layoutSeat = new LinearLayout(this);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutSeat.setOrientation(LinearLayout.VERTICAL);
        layoutSeat.setLayoutParams(params);
        layoutSeat.setPadding(8 * seatGaping, 8 * seatGaping, 8 * seatGaping, 8 * seatGaping);
        layout.addView(layoutSeat);

        LinearLayout layout = null;

        int count = 0;

        for (int index = 0; index < seats.length(); index++) {
            if (seats.charAt(index) == '/') {
                layout = new LinearLayout(this);
                layout.setOrientation(LinearLayout.HORIZONTAL);
                layoutSeat.addView(layout);
            } else if (seats.charAt(index) == 'U') {
                count++;
                TextView view = new TextView(this);
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(seatSize, seatSize);
                layoutParams.setMargins(seatGaping, seatGaping, seatGaping, seatGaping);
                view.setLayoutParams(layoutParams);
                view.setPadding(0, 0, 0, 2 * seatGaping);
                view.setId(count);
                view.setGravity(Gravity.CENTER);
                view.setBackgroundResource(R.drawable.ic_seats_booked);
                view.setTextColor(Color.WHITE);
                view.setTag(STATUS_BOOKED);
                view.setText(count + "");
                view.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 9);
                layout.addView(view);
                seatViewList.add(view);
                view.setOnClickListener(Reserve.this);
            } else if (seats.charAt(index) == 'A') {
                count++;
                TextView view = new TextView(this);
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(seatSize, seatSize);
                layoutParams.setMargins(seatGaping, seatGaping, seatGaping, seatGaping);
                view.setLayoutParams(layoutParams);
                view.setPadding(0, 0, 0, 2 * seatGaping);
                view.setId(count);
                view.setGravity(Gravity.CENTER);
                view.setBackgroundResource(R.drawable.ic_seats_book);
                // butaca = view.getText().toString();
                view.setText(count + "");
                view.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 9);
                view.setTextColor(Color.BLACK);
                view.setTag(STATUS_AVAILABLE);
                layout.addView(view);
                seatViewList.add(view);


                view.setOnClickListener(this);
            } else if (seats.charAt(index) == 'R') {
                count++;
                TextView view = new TextView(this);
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(seatSize, seatSize);
                layoutParams.setMargins(seatGaping, seatGaping, seatGaping, seatGaping);
                view.setLayoutParams(layoutParams);
                view.setPadding(0, 0, 0, 2 * seatGaping);
                view.setId(count);
                view.setGravity(Gravity.CENTER);
                view.setBackgroundResource(R.drawable.ic_seats_reserved);
                view.setText(count + "");
                view.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 9);
                view.setTextColor(Color.WHITE);
                view.setTag(STATUS_RESERVED);
                layout.addView(view);
                seatViewList.add(view);
                view.setOnClickListener(this);
            } else if (seats.charAt(index) == '_') {
                TextView view = new TextView(this);
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(seatSize, seatSize);
                layoutParams.setMargins(seatGaping, seatGaping, seatGaping, seatGaping);
                view.setLayoutParams(layoutParams);
                view.setBackgroundColor(Color.TRANSPARENT);
                view.setText("");
                layout.addView(view);
            }
        }


        mName = findViewById(R.id.tittle);

        cPicture = findViewById(R.id.img_cinema);
        mPicture = findViewById(R.id.img_movie);
        s1 = findViewById(R.id.sesion1);
        s2 = findViewById(R.id.sesion2);
        s3 = findViewById(R.id.sesion3);
        s4 = findViewById(R.id.sesion4);


        Intent intent = getIntent();
        idCinema = intent.getIntExtra("id_cinema", 0);
        idMovie = intent.getIntExtra("id", 0);
        tittle = intent.getStringExtra("name");
        ctiitle = intent.getStringExtra("cname");
        se1 = intent.getStringExtra("se1");
        se2 = intent.getStringExtra("se2");
        se3 = intent.getStringExtra("se3");
        se4 = intent.getStringExtra("se4");
        picturem = intent.getStringExtra("picture");
        picturec = intent.getStringExtra("foto1");
        idUser = intent.getStringExtra("email");


        mName.setText(tittle);
        RequestOptions requestOptions = new RequestOptions();
        requestOptions.skipMemoryCache(true);
        requestOptions.diskCacheStrategy(DiskCacheStrategy.NONE);
        requestOptions.placeholder(R.drawable.logo);
        requestOptions.error(R.drawable.logo);

        Glide.with(Reserve.this)
                .load(picturem)
                .apply(requestOptions)
                .into(mPicture);

        Glide.with(Reserve.this)
                .load(picturec)
                .apply(requestOptions)
                .into(cPicture);


        s1.setText(se1);
        s2.setText(se2);
        s3.setText(se3);
        s4.setText(se4);

        s1.setTextOn(se1);
        s1.setTextOff(se1);

        s2.setTextOn(se2);
        s2.setTextOff(se2);

        s3.setTextOn(se3);
        s3.setTextOff(se3);

        if (se4 == null) {

            s4.setVisibility(View.GONE);
        } else {
            s4.setTextOn(se4);
            s4.setTextOff(se4);
        }

        s1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {


                    s2.setChecked(false);
                    s3.setChecked(false);
                    s4.setChecked(false);
                    sesionselect = s1.getText().toString();

                } else {
                    // The toggle is disabled
                }
            }
        });
        s2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {

                    s1.setChecked(false);
                    s3.setChecked(false);
                    s4.setChecked(false);
                    sesionselect = s2.getText().toString();
                    // The toggle is enabled
                } else {
                    // The toggle is disabled
                }
            }
        });
        s3.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {

                    s2.setChecked(false);
                    s1.setChecked(false);
                    s4.setChecked(false);
                    sesionselect = s3.getText().toString();
                    // The toggle is enabled
                } else {
                    // The toggle is disabled
                }
            }
        });
        s4.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {

                    s2.setChecked(false);
                    s1.setChecked(false);
                    s3.setChecked(false);
                    sesionselect = s4.getText().toString();
                    // The toggle is enabled
                } else {
                    // The toggle is disabled
                }
            }
        });

        //obtener usuario

        appPreference = new AppPreference(this);

        Button reserveBtn = findViewById(R.id.btnReserve);

        //check login status from sharedPreference
        if (appPreference.getLoginStatus()) {
            //when true

            reserveBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {



                    SimpleDateFormat formato =
                            new SimpleDateFormat("dd-MM-yyyy -  HH:mm:ss", Locale.forLanguageTag("es-ES"));
                    String fecha = formato.format(new Date());






                    ///Reservar, insertar datos de reserva

                    if (sesionselect == null) {
                        Toast.makeText(Reserve.this, "Selecciona una sesión", Toast.LENGTH_SHORT).show();
                    } else {

                        apiInterface = ApiClient.getApiClient().create(API.class);
                        Call<Reserved> call = apiInterface.setReserve(idCinema, idMovie, sesionselect, idUser);
                        call.enqueue(new Callback<Reserved>() {
                            @Override
                            public void onResponse(Call<Reserved> call, Response<Reserved> response) {

                                Log.i(Reserve.class.getSimpleName(), response.body().toString());


                            }

                            @Override
                            public void onFailure(Call<Reserved> call, Throwable t) {
                                Toast.makeText(Reserve.this, "Fallo ", Toast.LENGTH_SHORT).show();
                            }
                        });


                        //Insertar los datos al ticket

                        Call<Ticket> insert = apiInterface.insertTicket(idUser, tittle, sesionselect, fecha, ctiitle, "" );
                        insert.enqueue(new Callback<Ticket>() {
                            @Override
                            public void onResponse(Call<Ticket> call, Response<Ticket> response) {

                                Log.i(Reserve.class.getSimpleName(), response.body().toString());


                            }

                            @Override
                            public void onFailure(Call<Ticket> call, Throwable t) {
                                Toast.makeText(Reserve.this, "Fallo ", Toast.LENGTH_SHORT).show();
                            }
                        });



                                QRBit = printQRCode("Película: " + tittle + " Cine: " + ctiitle + " Sesión: " + sesionselect + " Usuario: " + idUser);
                        if (QRBit == null) {
                            Toast.makeText(Reserve.this, "¡No ha sido posible generar la entrada!", Toast.LENGTH_SHORT).show();
                        } else {
                            Intent qRIntent = new Intent(Reserve.this, Tickets.class);

                            qRIntent.putExtra("bitmap", QRBit);
                            qRIntent.putExtra("name", tittle);
                            qRIntent.putExtra("picture", picturem);
                            qRIntent.putExtra("email", idUser);
                            qRIntent.putExtra("sesion", sesionselect);
                            qRIntent.putExtra("cname", ctiitle);
                            qRIntent.putExtra("butaca", myButaca(butaca));
                            qRIntent.putExtra("email", idUser);

                            startActivity(qRIntent);
                        }

                        saveTempBitmap(QRBit);
                        dirpath = Environment.getExternalStorageDirectory().toString();
                        qrPath = dirpath + "/TemporalQR/QR.jpg";
                        sendEmail(qrPath);
                    }}
            });


        } else {

            reserveBtn.setText("Iniciar sesión/ Registrarse");

        }


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        //añade menú
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_movie_detail, menu);

        return true;


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        item.setTitle("Mi perfil");
        if (id == R.id.action_cines) {
            Intent intent = new Intent(Reserve.this, CinemaList.class);
            startActivity(intent);
            return true;
        }

        if (id == R.id.action_peliculas) {
            Intent intent = new Intent(Reserve.this, MovieList.class);
            startActivity(intent);
            return true;
        }
        appPreference = new AppPreference(this);
        //check login status from sharedPreference
        if (appPreference.getLoginStatus()) {


            if (id == R.id.action_perfil) {


                Intent intent = new Intent(Reserve.this, Container.class);
                startActivity(intent);
                return true;
            }

        } else {
            item.setTitle("Iniciar sesión");
            if (id == R.id.action_perfil) {


                Intent intent = new Intent(Reserve.this, Container.class);
                startActivity(intent);
                return true;
            }
        }

        return super.onOptionsItemSelected(item);
    }

    private void sendEmail(String filename) {
        //Getting content for email

        String msgBody = "Hola! " + idUser +  " Esta es tu entrada para el cine: " + ctiitle + " Y la peícula " + tittle + " puedes escanaear el código QR";


        //Creating SendMail object
        SendMail sm = new SendMail(this, idUser, "Tu entrada en Cines Aragón " + tittle, msgBody, filename );

        //Executing sendmail to send email
        sm.execute();
    }

    //Generación de bitmap a imagen

    private Bitmap printQRCode(String textToQR) {
        MultiFormatWriter multiFormatWriter = new MultiFormatWriter();
        try {
            BitMatrix bitMatrix = multiFormatWriter.encode(textToQR, BarcodeFormat.QR_CODE, 300, 300);
            BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
            Bitmap bitmap = barcodeEncoder.createBitmap(bitMatrix);
            return bitmap;
        } catch (WriterException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void onClick(View view) {


        if ((int) view.getTag() == STATUS_AVAILABLE) {
            if (selectedIds.contains(view.getId() + ",")) {
                selectedIds = selectedIds.replace(+view.getId() + ",", "");
                view.setBackgroundResource(R.drawable.ic_seats_book);
            } else {
                selectedIds = selectedIds + view.getId() + ",";
                view.setBackgroundResource(R.drawable.ic_seats_selected);

                for (int i = 0; i < 10; i++) {
                    if (butaca.contains(view.getId())) {


                    } else {
                        butaca.add(i, view.getId());

                    }

                }


            }

            Toast.makeText(this, "Asiento " + view.getId() + " seleccionado", Toast.LENGTH_SHORT).show();


        } else if ((int) view.getTag() == STATUS_BOOKED) {
            Toast.makeText(this, "Seat " + view.getId() + " es comprado", Toast.LENGTH_SHORT).show();
        } else if ((int) view.getTag() == STATUS_RESERVED) {
            Toast.makeText(this, "Seat " + view.getId() + " está Reservado", Toast.LENGTH_SHORT).show();
        }


    }

    //Convierte el array list en string sin corchetes

    public String myButaca(ArrayList<Integer> strArray) {
        StringBuilder str = new StringBuilder();
        for (int i = 0; i < strArray.size(); i++) {
            int myNumbersInt = strArray.get(i);
            str.append(myNumbersInt + "-");
        }
        return str.toString();
    }

    public void saveTempBitmap(Bitmap bitmap) {
        if (isExternalStorageWritable()) {
            saveImage(bitmap);
        }else{
            //prompt the user or do something
        }
    }
    //TODO la puta fecha.
    private void saveImage(Bitmap finalBitmap) {

        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root + "/TemporalQR");
        myDir.mkdirs();


        String fname = "QR.jpg";

        File file = new File(myDir, fname);
        if (file.exists()) file.delete ();
        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /* Checks if external storage is available for read and write */
    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

}

