package com.seas.cinesaragon_seas.Vista;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.print.PrintHelper;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.seas.cinesaragon_seas.Controller.Extras.AppPreference;
import com.seas.cinesaragon_seas.Controller.Extras.Container;
import com.seas.cinesaragon_seas.Model.Ticket;
import com.seas.cinesaragon_seas.R;

public class Tickets extends AppCompatActivity {

    private TextView movietittle, cinematittle, sesiones, seats, userid, date;
    private String mtittle, ctittle, times, seat, picture, usermail, sesion, dates;
    private int movieiD, cinemaID;
    private ImageView mPicture, qR;
    private Bitmap qRBit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ticket);

        movietittle = findViewById(R.id.mtittle);
        cinematittle = findViewById(R.id.ctittle);
        sesiones = findViewById(R.id.sesions);
        userid = findViewById(R.id.userid);
        seats = findViewById(R.id.seats);
        mPicture = findViewById(R.id.imagemovie);
        qR = findViewById(R.id.qr);



        Intent tickedResume = getIntent();

        seat = tickedResume.getStringExtra("butaca");
        ctittle = tickedResume.getStringExtra("cname");
        dates = tickedResume.getStringExtra("date");
        mtittle = tickedResume.getStringExtra("name");
        picture = tickedResume.getStringExtra("picture");
        usermail = tickedResume.getStringExtra("email");
        sesion = tickedResume.getStringExtra("sesion");
        qRBit = tickedResume.getParcelableExtra("bitmap");

        String.join("[", seat);
        movietittle.setText(mtittle);
        cinematittle.setText(ctittle);
        sesiones.setText(sesion);
        userid.setText(usermail);
        seats.setText(seat);
        qR.setImageBitmap(qRBit);


        RequestOptions requestOptions = new RequestOptions();
        requestOptions.skipMemoryCache(true);
        requestOptions.diskCacheStrategy(DiskCacheStrategy.NONE);
        requestOptions.placeholder(R.drawable.logo);
        requestOptions.error(R.drawable.logo);

        Glide.with(Tickets.this)
                .load(picture)
                .apply(requestOptions)
                .into(mPicture);

        //mPicture.setText(mtittle);


        Button btnPrint, btnBack;

        btnPrint =  (Button) findViewById(R.id.btnprint);
        btnBack = findViewById(R.id.btnvolver);

        btnBack.setOnClickListener(new View.OnClickListener()  {
            @Override
            public void onClick(View view) {

               Intent inten = new Intent(Tickets.this, MovieList.class);
                inten.putExtra("email", usermail);
               startActivity(inten);
            }

        });

        btnPrint.setOnClickListener(new View.OnClickListener()  {
            @Override
            public void onClick(View view) {

                PrintHelper photoPrinter = new PrintHelper(Tickets.this);
                photoPrinter.setScaleMode(PrintHelper.SCALE_MODE_FIT);

                //print
                photoPrinter.printBitmap("image.png_test_print", qRBit, new PrintHelper.OnPrintFinishCallback() {
                    @Override
                    public void onFinish() {
                        Toast.makeText(Tickets.this, "¡Gracias!", Toast.LENGTH_SHORT).show();
                    }
                });

            }

        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        //añade menú
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_movie_detail, menu);


        return true;


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_cines) {
            Intent intent = new Intent(Tickets.this, CinemaList.class);
            intent.putExtra("email", usermail);
            startActivity(intent);
            return true;
        }

            // when get false
            if (id == R.id.action_perfil) {

                item.setTitle("Iniciar sesión");
                Intent intent = new Intent(Tickets.this, Container.class);
                intent.putExtra("email", usermail);
                startActivity(intent);
                return true;
            }


        return super.onOptionsItemSelected(item);
    }


}
