package com.seas.cinesaragon_seas.Vista;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.seas.cinesaragon_seas.Controller.Adapter.MovieAdapter;
import com.seas.cinesaragon_seas.Controller.Adapter.SlidingImage_Adapter;
import com.seas.cinesaragon_seas.Controller.Extras.Container;
import com.seas.cinesaragon_seas.Model.Movies;
import com.seas.cinesaragon_seas.Model.Remote.API;
import com.seas.cinesaragon_seas.Model.Trailers;
import com.seas.cinesaragon_seas.R;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;


public class CinemaDetails extends AppCompatActivity {

    private Spinner mGenderSpinner;
    private TextView mDir, mContact, mdDes;
    private ImageView mPicture, mPictures;
    private FloatingActionButton mFabChoosePic;
    private RecyclerView recyclerView;
    Vector<Trailers> trailers = new Vector<>();
    private RecyclerView.LayoutManager layoutManager;
    private MovieAdapter adapter;
    private List<Movies> moviesList;
    API apiInterface;
    MovieAdapter.RecyclerViewClickListener listener;
    ProgressBar progressBar;
    private String name, descripcion, foto1, foto2, foto3, direccion, contacto, userID;
    private int id;
    private Menu action;
    private Bitmap bitmap;
    private static ViewPager mPager;
    private static int currentPage = 0;
    private static int NUM_PAGES = 0;
    WebView browser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_cinema_detail);
//Se asignas los id de las etiquetas del layout de movie_details a las variables
        //mName = findViewById(R.id.);
        mdDes = findViewById(R.id.descripcion_cine);
        mContact = findViewById(R.id.dir_cine);
        mDir = findViewById(R.id.contacto_cine);



//traemos los datos pasados de la lista de poeliculas con getInExtra
        Intent intent = getIntent();
        id = intent.getIntExtra("id", 0);
        name = intent.getStringExtra("nombre");
        descripcion = intent.getStringExtra("descripcion");
        direccion = intent.getStringExtra("direccion");
        contacto = intent.getStringExtra("contacto");
        foto1 = intent.getStringExtra("picture1");
        foto2 = intent.getStringExtra("picture2");
        foto3 = intent.getStringExtra("picture3");
        userID = intent.getStringExtra("email");


//aplicamos los datos a las variables para asignar a la ficha de películas.

        //mName.setText(name);
        mdDes.setText(descripcion);
        mDir.setText(direccion);
        mContact.setText(contacto);
        String[] urls = new String[]{foto1, foto2, foto3};
        init(urls);


        Button btn = findViewById(R.id.btn_cienmas);

        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentPutID = new Intent(CinemaDetails.this, CinemaMovie.class);
                startActivityForResult(intentPutID, 0);
                intentPutID.putExtra("id", id);
                intentPutID.putExtra("email", userID);
                startActivity(intentPutID);

            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_movie_detail, menu);



        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {


        int id = item.getItemId();

        if (id == R.id.action_perfil) {
            Intent intent = new Intent(CinemaDetails.this, Container.class);
            intent.putExtra("email", userID);
            startActivity(intent);
            return true;
        } else if (id == R.id.action_cines) {
            Intent intent = new Intent(CinemaDetails.this, CinemaList.class);
            intent.putExtra("email", userID);
            startActivity(intent);
            return true;
        } else if (id == R.id.action_peliculas) {
            Intent intent = new Intent(CinemaDetails.this, MovieList.class);
            intent.putExtra("email", userID);
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void init(String[] urls) {

        mPager = (ViewPager) findViewById(R.id.pager);
        mPager.setAdapter(new SlidingImage_Adapter(CinemaDetails.this, urls));

        CirclePageIndicator indicator = (CirclePageIndicator)
                this.<View>findViewById(R.id.indicator);

        indicator.setViewPager(mPager);

        final float density = getResources().getDisplayMetrics().density;

//Set circle indicator radius
        indicator.setRadius(5 * density);

        NUM_PAGES = urls.length;

        // Auto start of viewpager
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (currentPage == NUM_PAGES) {
                    currentPage = 0;
                }
                mPager.setCurrentItem(currentPage++, true);
            }
        };
        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 10000, 10000);

        // Pager listener over indicator
        indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                currentPage = position;

            }

            @Override
            public void onPageScrolled(int pos, float arg1, int arg2) {

            }

            @Override
            public void onPageScrollStateChanged(int pos) {

            }
        });

        // Definimos el webView
        browser = (WebView) findViewById(R.id.webView);

        browser.setWebViewClient(new WebViewClient() {

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }
        });
        // Cargamos la web
        WebView webview = (WebView) findViewById(R.id.webView);
        webview.getSettings().setJavaScriptEnabled(true);
        browser.loadUrl("https://www.google.com/maps/search/?api=1&query=" + name.replace(" ", "+") + "%2C+zaragoza");
        //browser.loadUrl("https://www.google.com/maps/search/?api=1&query=Cine+Cervantes%2C+zaragoza");

    }


}
