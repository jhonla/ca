package com.seas.cinesaragon_seas.Vista;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;


import com.seas.cinesaragon_seas.Controller.Adapter.MovieAdapter;
import com.seas.cinesaragon_seas.Controller.Extras.Container;
import com.seas.cinesaragon_seas.Controller.Fragments.LoginFragment;
import com.seas.cinesaragon_seas.Model.Movies;
import com.seas.cinesaragon_seas.Model.Remote.API;
import com.seas.cinesaragon_seas.Model.Remote.ApiClient;
import com.seas.cinesaragon_seas.R;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CinemaMovie extends AppCompatActivity {
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private MovieAdapter adapter;
    private List<Movies> moviesList;
    API apiInterface;
    MovieAdapter.RecyclerViewClickListener listener;
    ProgressBar progressBar;
    private int idCinema;
    String idUsder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        apiInterface = ApiClient.getApiClient().create(API.class);
        progressBar = findViewById(R.id.progress);
        recyclerView = findViewById(R.id.recyclerView);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        Intent intent = getIntent();
        idCinema = intent.getIntExtra("id", 0);
        idUsder = intent.getStringExtra("email");

        listener = new MovieAdapter.RecyclerViewClickListener() {
            @Override
            public void onRowClick(View view, final int position) {

                Intent intent = new Intent(CinemaMovie.this, Reserve.class);
                intent.putExtra("id_cinema", moviesList.get(position).getId());
                intent.putExtra("name", moviesList.get(position).getNombre());
                intent.putExtra("descripcion", moviesList.get(position).getDescripcion());
                intent.putExtra("picture", moviesList.get(position).getPicture());
                intent.putExtra("trailer", moviesList.get(position).getTrailer());
                intent.putExtra("se1", moviesList.get(position).getSe1());
                intent.putExtra("se2", moviesList.get(position).getSe2());
                intent.putExtra("se3", moviesList.get(position).getSe3());
                intent.putExtra("se4", moviesList.get(position).getSe4());
                intent.putExtra("app", moviesList.get(position).getSe4());
                intent.putExtra("email", idUsder);
                startActivity(intent);
            }

        };


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        //añade menú
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_movie_detail, menu);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        final SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        MenuItem searchMenuItem = menu.findItem(R.id.action_search);

        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName())
        );
        searchView.setQueryHint("Buscar película...");
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(final String query) {

                adapter.getFilter().filter(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                adapter.getFilter().filter(newText);
                return false;
            }
        });

        searchMenuItem.getIcon().setVisible(false, false);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_cines) {
            Intent intent = new Intent(CinemaMovie.this, CinemaList.class);
            intent.putExtra("email", idUsder);
            startActivity(intent);

            return true;
        }
        if (id == R.id.action_peliculas) {
            Intent intent = new Intent(CinemaMovie.this, MovieList.class);
            intent.putExtra("email", idUsder);
            startActivity(intent);
            return true;
        }
        if (id == R.id.action_perfil) {
            Intent intent = new Intent(CinemaMovie.this, Container.class);
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);

    }

    public void getMovies() {

        Call<List<Movies>> call = apiInterface.getMoviesByIcinemaID(idCinema);
        call.enqueue(new Callback<List<Movies>>() {
            @Override
            public void onResponse(Call<List<Movies>> call, Response<List<Movies>> response) {
                progressBar.setVisibility(View.GONE);
                moviesList = response.body();
                Log.i(CinemaMovie.class.getSimpleName(), response.body().toString());
                adapter = new MovieAdapter(moviesList, CinemaMovie.this, listener);
                recyclerView.setAdapter(adapter);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<List<Movies>> call, Throwable t) {
                Toast.makeText(CinemaMovie.this, "rp :" +
                                t.getMessage().toString(),
                        Toast.LENGTH_SHORT).show();
            }
        });
    }


    @Override
    protected void onResume() {
        super.onResume();
        getMovies();
    }

}
