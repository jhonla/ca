package com.seas.cinesaragon_seas.Vista;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;


import com.seas.cinesaragon_seas.Controller.Adapter.CinemaMAdapeter;
import com.seas.cinesaragon_seas.Controller.Extras.Container;
import com.seas.cinesaragon_seas.Model.Cinemas;
import com.seas.cinesaragon_seas.Model.Remote.API;
import com.seas.cinesaragon_seas.Model.Remote.ApiClient;
import com.seas.cinesaragon_seas.R;


import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MovieCinema extends AppCompatActivity {

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private CinemaMAdapeter adapter;
    private List<Cinemas> cinemasList;
    API apiInterface;
    CinemaMAdapeter.RecyclerViewClickListener listener;
    ProgressBar progressBar;
    private int idMovie;
    private String tittle, picture, userId;
    private TextView s1;
    private TextView s2;
    private TextView s3;
    private TextView s4;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_cinema);

        apiInterface = ApiClient.getApiClient().create(API.class);
        progressBar = findViewById(R.id.progress);
        recyclerView = findViewById(R.id.recyclerView);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);



        Intent intent = getIntent();
        idMovie = intent.getIntExtra("id", 0);
        tittle = intent.getStringExtra("name");
        picture = intent.getStringExtra("picture");
        userId = intent.getStringExtra("email");


        listener = new CinemaMAdapeter.RecyclerViewClickListener() {
            @Override
            public void onRowClick(View view, final int position) {

                Intent intent = new Intent(MovieCinema.this, Reserve.class);
                intent.putExtra("id_cinema", cinemasList.get(position).getId());
                intent.putExtra("cname", cinemasList.get(position).getNombre());
                intent.putExtra("se1", cinemasList.get(position).getSe1());
                intent.putExtra("se2", cinemasList.get(position).getSe2());
                intent.putExtra("se3", cinemasList.get(position).getSe3());
                intent.putExtra("se4", cinemasList.get(position).getSe4());
                intent.putExtra("foto1", cinemasList.get(position).getFoto1());
                intent.putExtra("picture",picture);
                intent.putExtra("name", tittle);
                intent.putExtra("id", idMovie);
                intent.putExtra("email", userId);

                startActivity(intent);
            }

        };



    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_movie_detail, menu);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        final SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        MenuItem searchMenuItem = menu.findItem(R.id.action_search);

        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName())
        );
        searchView.setQueryHint("Buscar cine...");
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(final String query) {

                adapter.getFilter().filter(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                adapter.getFilter().filter(newText);
                return false;
            }
        });

        searchMenuItem.getIcon().setVisible(false, false);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_perfil) {
            Intent intent = new Intent(MovieCinema.this, Container.class);
            intent.putExtra("email", userId);
            startActivity(intent);
            return true;
        }

        else if (id == R.id.action_peliculas) {
            Intent intent = new Intent(MovieCinema.this, MovieList.class);
            intent.putExtra("email", userId);

            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }



    public void getCinemas(){

        Call<List<Cinemas>> call = apiInterface.getCinemaBymovieID(idMovie);
        call.enqueue(new Callback<List<Cinemas>>() {
            @Override
            public void onResponse(Call<List<Cinemas>> call, Response<List<Cinemas>> response) {
                progressBar.setVisibility(View.GONE);
                cinemasList = response.body();
                Log.i(CinemaList.class.getSimpleName(), response.body().toString());
                adapter = new CinemaMAdapeter(cinemasList, MovieCinema.this, listener);
                recyclerView.setAdapter(adapter);
                adapter.notifyDataSetChanged();

            }

            @Override
            public void onFailure(Call<List<Cinemas>> call, Throwable t) {
                Toast.makeText(MovieCinema.this, "rp :"+
                                t.getMessage().toString(),
                        Toast.LENGTH_SHORT).show();
            }
        });
    }


    @Override
    protected void onResume() {
        super.onResume();
        getCinemas();
    }
}
