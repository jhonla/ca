package com.seas.cinesaragon_seas.Vista;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;


import com.seas.cinesaragon_seas.Controller.Adapter.CinemaAdapeter;
import com.seas.cinesaragon_seas.Controller.Extras.Container;
import com.seas.cinesaragon_seas.Model.Cinemas;
import com.seas.cinesaragon_seas.Model.Remote.API;
import com.seas.cinesaragon_seas.Model.Remote.ApiClient;
import com.seas.cinesaragon_seas.R;


import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CinemaList extends AppCompatActivity {

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private CinemaAdapeter adapter;
    private List<Cinemas> cinemasList;
    API apiInterface;
    CinemaAdapeter.RecyclerViewClickListener listener;
    ProgressBar progressBar;
    String userID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cinemas);

        apiInterface = ApiClient.getApiClient().create(API.class);
        progressBar = findViewById(R.id.progress);
        recyclerView = findViewById(R.id.recyclerView);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        Intent userIdIntent = getIntent();
        userID = userIdIntent.getStringExtra("email");


        listener = new CinemaAdapeter.RecyclerViewClickListener() {
            @Override
            public void onRowClick(View view, final int position) {

                Intent intent = new Intent(CinemaList.this, CinemaDetails.class);
                intent.putExtra("id", cinemasList.get(position).getId());
                intent.putExtra("nombre", cinemasList.get(position).getNombre());
                intent.putExtra("descripcion", cinemasList.get(position).getDescripcion());
                intent.putExtra("direccion", cinemasList.get(position).getDireccion());
                intent.putExtra("contacto", cinemasList.get(position).getContacto());
                intent.putExtra("picture1", cinemasList.get(position).getFoto1());
                intent.putExtra("picture2", cinemasList.get(position).getFoto2());
                intent.putExtra("picture3", cinemasList.get(position).getFoto3());
                intent.putExtra("email", userID);

                startActivity(intent);
            }

        };


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_movie_detail, menu);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        final SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        MenuItem searchMenuItem = menu.findItem(R.id.action_search);

        searchView.setSearchableInfo(
                searchManager.getSearchableInfo(getComponentName())
        );
        searchView.setQueryHint("Buscar cine...");
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(final String query) {

                adapter.getFilter().filter(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                adapter.getFilter().filter(newText);
                return false;
            }
        });

        searchMenuItem.getIcon().setVisible(false, false);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_perfil) {
            Intent intent = new Intent(CinemaList.this, Container.class);
            intent.putExtra("email", userID);
            startActivity(intent);
            return true;
        } else if (id == R.id.action_peliculas) {
            Intent intent = new Intent(CinemaList.this, MovieList.class);
            intent.putExtra("email", userID);
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void getCinemas() {

        Call<List<Cinemas>> call = apiInterface.getCinemas();
        call.enqueue(new Callback<List<Cinemas>>() {
            @Override
            public void onResponse(Call<List<Cinemas>> call, Response<List<Cinemas>> response) {
                progressBar.setVisibility(View.GONE);
                cinemasList = response.body();
                Log.i(CinemaList.class.getSimpleName(), response.body().toString());
                adapter = new CinemaAdapeter(cinemasList, CinemaList.this, listener);
                recyclerView.setAdapter(adapter);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<List<Cinemas>> call, Throwable t) {
                Toast.makeText(CinemaList.this, "rp :" +
                                t.getMessage().toString(),
                        Toast.LENGTH_SHORT).show();
            }
        });
    }


    @Override
    protected void onResume() {
        super.onResume();
        getCinemas();
    }

}
