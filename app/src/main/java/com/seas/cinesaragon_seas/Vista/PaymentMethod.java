package com.seas.cinesaragon_seas.Vista;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.seas.cinesaragon_seas.Controller.Extras.Container;
import com.seas.cinesaragon_seas.Model.User;
import com.seas.cinesaragon_seas.R;
import com.seas.cinesaragon_seas.Model.Remote.API;
import com.seas.cinesaragon_seas.Model.Remote.ApiClient;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PaymentMethod extends AppCompatActivity {

    private EditText n, ccv, date;
    private String email, nCard, ccvCard, expireCard;
    API apiInterface;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_method);


        Intent getUser = getIntent();
        email = getUser.getStringExtra("userId");

        n = findViewById(R.id.tarjeta);
        ccv = findViewById(R.id.ccv);
        date = findViewById(R.id.vencimiento);


        Button btnUpdate = findViewById(R.id.btn_update);

        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                apiInterface = ApiClient.getApiClient().create(API.class);
                nCard = n.getText().toString();
                ccvCard = ccv.getText().toString();
                expireCard = date.getText().toString();

                Call<User> update = apiInterface.UpdatePayment(email, nCard, ccvCard, expireCard);
                update.enqueue(new Callback<User>() {
                    @Override
                    public void onResponse(Call<User> call, Response<User> response) {

                        Log.i(PaymentMethod.class.getSimpleName(), response.body().toString());

                        Toast.makeText(PaymentMethod.this, "Se ha actualizado la forma de pago ", Toast.LENGTH_SHORT).show();
                    }


                    @Override
                    public void onFailure(Call<User> call, Throwable t) {
                        Toast.makeText(PaymentMethod.this, "Fallo ", Toast.LENGTH_SHORT).show();
                    }
                });




                Intent intent = new Intent(PaymentMethod.this, Container.class);

                intent.putExtra("newCard", nCard);
                startActivity(intent);
            }
        });
    }
}
