package com.seas.cinesaragon_seas.Model.Remote;

public interface SesionInterface {

    // for login
    void register();
    void login(String email, String pass, String t);
    void logout();

}
