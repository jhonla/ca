package com.seas.cinesaragon_seas.Model;

import com.google.gson.annotations.SerializedName;

public class Trailers {

   public String videoUrl;

    public Trailers() {
    }

    public Trailers(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }



}