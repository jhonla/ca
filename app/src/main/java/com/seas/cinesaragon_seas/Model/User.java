package com.seas.cinesaragon_seas.Model;

import com.google.gson.annotations.SerializedName;

public class User {

    @SerializedName("response")
    private String response;

    @SerializedName("id")
    private String useriD;

    @SerializedName("name")
    private String name;

    @SerializedName("email")
    private String email;

    public String getUseriD() {
        return useriD;
    }

    public void setUseriD(String useriD) {
        this.useriD = useriD;
    }

    @SerializedName("numero")
    private String numero;

    @SerializedName("CVV")
    private String CVV;

    @SerializedName("caducidad")
    private String caducidad;


    public String getResponse() {
        return response;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getCVV() {
        return CVV;
    }

    public void setCVV(String CVV) {
        this.CVV = CVV;
    }

    public String getCaducidad() {
        return caducidad;
    }

    public void setCaducidad(String caducidad) {
        this.caducidad = caducidad;
    }

    @Override
    public String toString() {
        return "User{" +
                "response='" + response + '\'' +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}
