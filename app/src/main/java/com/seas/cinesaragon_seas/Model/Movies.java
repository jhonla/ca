package com.seas.cinesaragon_seas.Model;

import com.google.gson.annotations.SerializedName;

public class Movies   {


    @SerializedName("id")
    private int id;
    @SerializedName("nombre")
    private String nombre;
    @SerializedName("descripcion")
    private String descripcion;
    @SerializedName("duracion")
    private String duracion;
    @SerializedName("se1")
    private String se1;
    @SerializedName("se2")
    private String se2;
    @SerializedName("se3")
    private String se3;
    @SerializedName("se4")
    private String se4;
    @SerializedName("picture")
    private String picture;
    @SerializedName("trailer")
    private String trailer;
    @SerializedName("value")
    private String value;
    @SerializedName("message")
    private String massage;

    public String getSe1() {
        return se1;
    }

    public void setSe1(String se1) {
        this.se1 = se1;
    }

    public String getSe2() {
        return se2;
    }

    public void setSe2(String se2) {
        this.se2 = se2;
    }

    public String getSe3() {
        return se3;
    }

    public void setSe3(String se3) {
        this.se3 = se3;
    }

    public String getSe4() {
        return se4;
    }

    public void setSe4(String se4) {
        this.se4 = se4;
    }

    public String getTrailer() {
        return trailer;
    }

    public void setTrailer(String trailer) {
        this.trailer = trailer;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDuracion() {
        return duracion;
    }

    public void setDuracion(String duracion) {
        this.duracion = duracion;
    }


    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }


    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getMassage() {
        return massage;
    }

    public void setMassage(String massage) {
        this.massage = massage;
    }
}
