package com.seas.cinesaragon_seas.Model.Remote;

import android.util.Log;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {

    /** Constants values are here */

        public final static String BASE_URL = "https://44.ip-51-75-22.eu/";




    public static Retrofit getApiClient(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        Log.e("RetrofitClient", BASE_URL);


        return retrofit;
    }

    }

