package com.seas.cinesaragon_seas.Model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Cinemas {


    @SerializedName("id_cinema")
    private int id;
    @SerializedName("nombre")
    private String nombre;
    @SerializedName("descripcion")
    private String descripcion;
    @SerializedName("latitud")
    private String latitud;
    @SerializedName("longitud")
    private String longitud;
    @SerializedName("sala1")
    private String sala1;
    @SerializedName("sala2")
    private String sala2;
    @SerializedName("sala3")
    private String sala3;
    @SerializedName("sala4")
    private String sala4;
    @SerializedName("pelicula1")
    private String pelicula1;
    @SerializedName("pelicula2")
    private String pelicula2;
    @SerializedName("pelicula3")
    private String pelicula3;
    @SerializedName("pelicula4")
    private String pelicula4;
    @SerializedName("foto1")
    private String foto1;
    @SerializedName("foto2")
    private String foto2;
    @SerializedName("foto3")
    private String foto3;
    @SerializedName("direccion")
    private String direccion;
    @SerializedName("contacto")
    private String contacto;

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getContacto() {
        return contacto;
    }

    public void setContacto(String contacto) {
        this.contacto = contacto;
    }

    @SerializedName("message")
    private String massage;

    @SerializedName("se1")
    private String se1;

    @SerializedName("se2")
    private String se2;

    @SerializedName("se3")
    private String se3;

    @SerializedName("se4")
    private String se4;


    public String getSe1() {
        return se1;
    }

    public void setSe1(String se1) {
        this.se1 = se1;
    }

    public String getSe2() {
        return se2;
    }

    public void setSe2(String se2) {
        this.se2 = se2;
    }

    public String getSe3() {
        return se3;
    }

    public void setSe3(String se3) {
        this.se3 = se3;
    }

    public String getSe4() {
        return se4;
    }

    public void setSe4(String se4) {
        this.se4 = se4;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }


    public String getLatitud() {
        return latitud;
    }

    public void setLatitud(String latitud) {
        this.latitud = latitud;
    }

    public String getLongitud() {
        return longitud;
    }

    public void setLongitud(String longitud) {
        this.longitud = longitud;
    }

    public String getSala1() {
        return sala1;
    }

    public void setSala1(String sala1) {
        this.sala1 = sala1;
    }

    public String getSala2() {
        return sala2;
    }

    public void setSala2(String sala2) {
        this.sala2 = sala2;
    }

    public String getSala3() {
        return sala3;
    }

    public void setSala3(String sala3) {
        this.sala3 = sala3;
    }

    public String getSala4() {
        return sala4;
    }

    public void setSala4(String sala4) {
        this.sala4 = sala4;
    }

    public String getPelicula1() {
        return pelicula1;
    }

    public void setPelicula1(String pelicula1) {
        this.pelicula1 = pelicula1;
    }

    public String getPelicula2() {
        return pelicula2;
    }

    public void setPelicula2(String pelicula2) {
        this.pelicula2 = pelicula2;
    }

    public String getPelicula3() {
        return pelicula3;
    }

    public void setPelicula3(String pelicula3) {
        this.pelicula3 = pelicula3;
    }

    public String getPelicula4() {
        return pelicula4;
    }

    public void setPelicula4(String pelicula4) {
        this.pelicula4 = pelicula4;
    }

    public String getFoto1() {
        return foto1;
    }

    public void setFoto1(String foto1) {
        this.foto1 = foto1;
    }

    public String getFoto2() {
        return foto2;
    }

    public void setFoto2(String foto2) {
        this.foto2 = foto2;
    }

    public String getFoto3() {
        return foto3;
    }

    public void setFoto3(String foto3) {
        this.foto3 = foto3;
    }

    public String getMassage() {
        return massage;
    }

    public void setMassage(String massage) {
        this.massage = massage;
    }
}
