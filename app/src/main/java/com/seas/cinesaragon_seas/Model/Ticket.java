package com.seas.cinesaragon_seas.Model;

import com.google.gson.annotations.SerializedName;

public class Ticket {

    @SerializedName("usuario")
    private String usuario;

    @SerializedName("pelicula")
    private String pelicula;

    @SerializedName("sesion")
    private String sesion;

    @SerializedName("date")
    private String date;

    @SerializedName("cine")
    private String cine;


    @SerializedName("qr")
    private String qr;

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getPelicula() {
        return pelicula;
    }

    public void setPelicula(String pelicula) {
        this.pelicula = pelicula;
    }

    public String getSesion() {
        return sesion;
    }

    public void setSesion(String sesion) {
        this.sesion = sesion;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getCine() {
        return cine;
    }

    public void setCine(String cine) {
        this.cine = cine;
    }

    public String getQr() {
        return qr;
    }

    public void setQr(String qr) {
        this.qr = qr;
    }
}
