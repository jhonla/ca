package com.seas.cinesaragon_seas.Model.Remote;

import com.seas.cinesaragon_seas.Model.Cinemas;
import com.seas.cinesaragon_seas.Model.Movies;
import com.seas.cinesaragon_seas.Model.Reserved;
import com.seas.cinesaragon_seas.Model.Ticket;
import com.seas.cinesaragon_seas.Model.User;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface API {
    @FormUrlEncoded
    @POST("register.php")
    Call<User> register(
            @Field("name") String name,
            @Field("email") String email,
            @Field("password") String password);

    @FormUrlEncoded
    @POST("login.php")
    Call<User> login(

            @Field("email") String email,
            @Field("password") String password
    );

    @POST("get_movies.php")
    Call<List<Movies>> getMovies();

    @POST("get_cinemas.php")
    Call<List<Cinemas>> getCinemas();

    @FormUrlEncoded
    @POST("get_cinemaBymovie.php")
    Call<List<Cinemas>> getCinemaBymovieID(@Field("id") int id);

    @FormUrlEncoded
    @POST("get_moviesBycinema.php")
    Call<List<Movies>> getMoviesByIcinemaID(@Field("id") int id);

    @FormUrlEncoded
    @POST("reservar.php")
    Call<Reserved> setReserve(
            @Field("id_cine") int id_cine,
            @Field("id_movie") int id_movie,
            @Field("sesion") String sesion,
            @Field("id_user") String userID);

    @FormUrlEncoded
    @POST("get_tickets.php")
    Call<List<Ticket>> getTickets(@Field("userId") String email);

    @FormUrlEncoded
    @POST("register_ticket.php")
    Call<Ticket> insertTicket(

            @Field("usuario") String usuario,
            @Field("pelicula") String pelicula,
            @Field("sesion") String sesion,
            @Field("date") String date,
            @Field("cine") String cine,
            @Field("QR") String qr);

    @FormUrlEncoded
    @POST("update_user_details.php")
    Call<User> UpdatePayment(

            @Field("email") String email,
            @Field("numero") String numero,
            @Field("CCV") String CCV,
            @Field("caducidad") String caducidad);




    @FormUrlEncoded
    @POST("get_acount.php")
    Call<User> getAcount(

            @Field("email") String email);



}

