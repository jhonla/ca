package com.seas.cinesaragon_seas.Controller.Fragments;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.seas.cinesaragon_seas.Controller.Extras.Container;

import com.seas.cinesaragon_seas.Model.Remote.SesionInterface;
import com.seas.cinesaragon_seas.Model.User;
import com.seas.cinesaragon_seas.R;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class RegistrationFragment extends Fragment {

    private EditText nameInput, emailInput, passwordInput;
    Button regBtn;
    SesionInterface loginFromActivityListener;

    public RegistrationFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_registration, container, false);
        nameInput = view.findViewById(R.id.nameInput);
        emailInput = view.findViewById(R.id.emailInput);
        passwordInput = view.findViewById(R.id.passwordInput);
        regBtn = view.findViewById(R.id.regBtn);
        regBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                registerUser();
                Log.e("reg button", "clicked");
            }
        });
        return view;
    }

    public void registerUser() {
        String name = nameInput.getText().toString();
        String email = emailInput.getText().toString();
        String password = passwordInput.getText().toString();

        if (TextUtils.isEmpty(name)){
            Container.appPreference.showToast("Ingresa un nombre.");
        } else if (TextUtils.isEmpty(email)){
            Container.appPreference.showToast("Ingresa un correo.");
        } else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            Container.appPreference.showToast("correo incorrecto");
        } else if (TextUtils.isEmpty(password)){
            Container.appPreference.showToast("Ingresa una contraseña");
        } else if (password.length() < 6){
            Container.appPreference.showToast("la contraseña debe tener mínimo 6 carácteres.");
        }
        else {
            Call<User> userCall = Container.serviceApi.register(name, email, password);
            userCall.enqueue(new Callback<User>() {
                @Override
                public void onResponse(Call<User> call, Response<User> response) {
                    if (response.body().getResponse().equals("inserted")){
                        Log.e("response", response.body().getResponse());
                        nameInput.setText("");
                        emailInput.setText("");
                        passwordInput.setText("");
                        Container.appPreference.showToast("Registro correcto");
                        Intent intent = new Intent(getActivity(), Container.class);
                        startActivity(intent);
                    } else if (response.body().getResponse().equals("exists")){
                        Container.appPreference.showToast("Este correo ya existe");
                    } else if (response.body().getResponse().equals("error")){
                        Container.appPreference.showToast("Algo ha ido mal.");
                    }
                }

                @Override
                public void onFailure(Call<User> call, Throwable t) {
                }
            });
        }

    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Activity activity = (Activity) context;
        loginFromActivityListener = (SesionInterface) activity;
    }


}
