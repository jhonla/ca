package com.seas.cinesaragon_seas.Controller.Extras;

import android.widget.Filter;

import com.seas.cinesaragon_seas.Controller.Adapter.TicketAdapter;
import com.seas.cinesaragon_seas.Model.Ticket;

import java.util.ArrayList;

public class TicketFilter extends Filter {

    TicketAdapter adapter;
    ArrayList<Ticket> filterList;

    public TicketFilter(ArrayList<Ticket> filterList, TicketAdapter adapter)
    {
        this.adapter=adapter;
        this.filterList=filterList;

    }

    //FILTERING OCURS
    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        FilterResults results=new FilterResults();
        //CHECK CONSTRAINT VALIDITY
        if(constraint != null && constraint.length() > 0)
        {
            //CHANGE TO UPPER
            constraint=constraint.toString().toUpperCase();
            //STORE OUR FILTERED PLAYERS
            ArrayList<Ticket> filteredPets=new ArrayList<>();

            for (int i=0;i<filterList.size();i++)
            {
                //CHECK
                if(filterList.get(i).getPelicula().toUpperCase().contains(constraint))
                {
                    //ADD PLAYER TO FILTERED PLAYERS
                    filteredPets.add(filterList.get(i));
                }
            }

            results.count=filteredPets.size();
            results.values=filteredPets;

        }else
        {
            results.count=filterList.size();
            results.values=filterList;
        }

        return results;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {

        adapter.tickets= (ArrayList<Ticket>) results.values;

        //REFRESH
        adapter.notifyDataSetChanged();

    }
}
