package com.seas.cinesaragon_seas.Controller.Extras;

import android.widget.Filter;

import com.seas.cinesaragon_seas.Controller.Adapter.MovieAdapter;
import com.seas.cinesaragon_seas.Model.Movies;

import java.util.ArrayList;

public class MovieFilter extends Filter {

    MovieAdapter adapter;
    ArrayList<Movies> filterList;

    public MovieFilter(ArrayList<Movies> filterList, MovieAdapter adapter)
    {
        this.adapter=adapter;
        this.filterList=filterList;

    }

    //FILTERING OCURS
    @Override
    protected Filter.FilterResults performFiltering(CharSequence constraint) {
        Filter.FilterResults results=new Filter.FilterResults();
        //CHECK CONSTRAINT VALIDITY
        if(constraint != null && constraint.length() > 0)
        {
            //CHANGE TO UPPER
            constraint=constraint.toString().toUpperCase();
            //STORE OUR FILTERED PLAYERS
            ArrayList<Movies> filteredPets=new ArrayList<>();

            for (int i=0;i<filterList.size();i++)
            {
                //CHECK
                if(filterList.get(i).getNombre().toUpperCase().contains(constraint))
                {
                    //ADD PLAYER TO FILTERED PLAYERS
                    filteredPets.add(filterList.get(i));
                }
            }

            results.count=filteredPets.size();
            results.values=filteredPets;

        }else
        {
            results.count=filterList.size();
            results.values=filterList;
        }

        return results;
    }

    @Override
    protected void publishResults(CharSequence constraint, Filter.FilterResults results) {

        adapter.movies= (ArrayList<Movies>) results.values;

        //REFRESH
        adapter.notifyDataSetChanged();

    }
}
