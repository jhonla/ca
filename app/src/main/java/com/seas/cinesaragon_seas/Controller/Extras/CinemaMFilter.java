package com.seas.cinesaragon_seas.Controller.Extras;

import android.widget.Filter;

import com.seas.cinesaragon_seas.Controller.Adapter.CinemaMAdapeter;
import com.seas.cinesaragon_seas.Model.Cinemas;

import java.util.ArrayList;

public class CinemaMFilter extends Filter {

    CinemaMAdapeter adapter;
    ArrayList<Cinemas> filterList;

    public CinemaMFilter(ArrayList<Cinemas> filterList, CinemaMAdapeter adapter)
    {
        this.adapter=adapter;
        this.filterList=filterList;
        /* TODO */
    }

    //FILTERING OCURS
    @Override
    protected FilterResults performFiltering(CharSequence constraint) {
        FilterResults results=new FilterResults();
        //CHECK CONSTRAINT VALIDITY
        if(constraint != null && constraint.length() > 0)
        {
            //CHANGE TO UPPER
            constraint=constraint.toString().toUpperCase();
            //STORE OUR FILTERED PLAYERS
            ArrayList<Cinemas> filteredcinemas=new ArrayList<>();

            for (int i=0;i<filterList.size();i++)
            {
                //CHECK
                if(filterList.get(i).getNombre().toUpperCase().contains(constraint))
                {
                    //ADD PLAYER TO FILTERED PLAYERS
                    filteredcinemas.add(filterList.get(i));
                }
            }

            results.count=filteredcinemas.size();
            results.values=filteredcinemas;

        }else
        {
            results.count=filterList.size();
            results.values=filterList;
        }

        return results;
    }

    @Override
    protected void publishResults(CharSequence constraint, FilterResults results) {

        adapter.cinemas= (ArrayList<Cinemas>) results.values;

        //REFRESH
        adapter.notifyDataSetChanged();

    }
}
