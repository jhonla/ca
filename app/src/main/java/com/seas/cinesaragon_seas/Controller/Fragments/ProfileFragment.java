package com.seas.cinesaragon_seas.Controller.Fragments;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.seas.cinesaragon_seas.Model.Remote.API;
import com.seas.cinesaragon_seas.Model.Remote.SesionInterface;
import com.seas.cinesaragon_seas.Vista.CinemaList;
import com.seas.cinesaragon_seas.Vista.MovieList;
import com.seas.cinesaragon_seas.Vista.PaymentMethod;
import com.seas.cinesaragon_seas.Vista.TicketList;
import com.seas.cinesaragon_seas.Controller.Extras.AppPreference;
import com.seas.cinesaragon_seas.Controller.Extras.Container;
import com.seas.cinesaragon_seas.Model.User;
import com.seas.cinesaragon_seas.R;


import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends Fragment {

    private SesionInterface loginFromActivityListener;
    private TextView saludo, email, userName, tarjeta;
    private String userId;
    String userEmail;
    String myStr;
    SesionInterface logoutListener;
    API apiInterface;

    public ProfileFragment() {
        //
    }

    Button logoutBtn, updatetBtn, ticketstBtn;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_profile, container, false);


        saludo = view.findViewById(R.id.saludo);
        email = view.findViewById(R.id.pemail);
        userName = view.findViewById(R.id.pusername);
        tarjeta = view.findViewById(R.id.card);


        //bienvenida
        String welcome = "Hola, " + Container.appPreference.getDisplayName();
        saludo.setText(welcome);

        email.setText(Container.appPreference.getDisplayEmail());
        userName.setText(Container.appPreference.getDisplayName());
        userEmail = Container.appPreference.getDisplayEmail();
        Call<User> userCall = Container.serviceApi.getAcount(userEmail);
        userCall.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if (response.body().getResponse().equals("data")) {
                    Container.appPreference.setLoginStatus(true); // set login status in sharedPreference


                            response.body().getNumero();
                    // response.body().getCaducidad(),
                    //  response.body().getCVV());


                    Container.appPreference.setDisplayCard(response.body().getNumero());
                    //  Container.appPreference.setDisplayf( response.body().getCaducidad());
                    //  Container.appPreference.setDisplayccv( response.body().getCVV());
                    tarjeta.setText(Container.appPreference.getDisplayCard());

                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Container.appPreference.showToast("Error. No conecta con el PHP");
            }


        });


        userId = Container.appPreference.getDisplayEmail();



       /* date.setText(Container.appPreference.getDisplayt());
        ccv.setText(Container.appPreference.getDisplayccv());*/
        ticketstBtn = view.findViewById(R.id.btn_tickets);

        updatetBtn = view.findViewById(R.id.btn_update);

        updatetBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(getActivity(), PaymentMethod.class);
                intent.putExtra("userId", userEmail);
                startActivity(intent);
            }
        });


        ticketstBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), TicketList.class);
                intent.putExtra("userId", userEmail);

                startActivity(intent);
            }
        });


        logoutBtn = view.findViewById(R.id.btn_close);
        logoutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                logoutListener.logout();
            }
        });

        return view;
    } // ending onCreateView

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Activity activity = (Activity) context;
        logoutListener = (SesionInterface) activity;

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_movie_detail, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_cines) {
            Intent intent = new Intent(getActivity(), CinemaList.class);
            intent.putExtra("email", Container.appPreference.getDisplayEmail());
            startActivity(intent);
            return true;
        }
        if (id == R.id.action_peliculas) {
            Intent intent = new Intent(getActivity(), MovieList.class);
            intent.putExtra("email", userEmail);
            startActivity(intent);
            return true;
        }
        Container.appPreference = new AppPreference(getActivity());
        //check login status from sharedPreference
        if (Container.appPreference.getLoginStatus()) {


            if (id == R.id.action_perfil) {

                item.setTitle("Mi perfil");
                Intent intent = new Intent(getActivity(), Container.class);
                startActivity(intent);
                return true;
            }

        } else {
            // when get false
            if (id == R.id.action_perfil) {

                item.setTitle("Iniciar sesión");
                Intent intent = new Intent(getActivity(), Container.class);
                startActivity(intent);
                return true;
            }
        }

        return super.onOptionsItemSelected(item);
    }


}


