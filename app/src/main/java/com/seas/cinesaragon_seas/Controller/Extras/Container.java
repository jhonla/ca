package com.seas.cinesaragon_seas.Controller.Extras;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.FrameLayout;

import com.seas.cinesaragon_seas.Controller.Fragments.LoginFragment;
import com.seas.cinesaragon_seas.Controller.Fragments.RegistrationFragment;
import com.seas.cinesaragon_seas.Controller.Fragments.ProfileFragment;
import com.seas.cinesaragon_seas.Model.Remote.API;
import com.seas.cinesaragon_seas.Model.Remote.ApiClient;
import com.seas.cinesaragon_seas.Model.Remote.SesionInterface;
import com.seas.cinesaragon_seas.R;



public class Container extends AppCompatActivity implements SesionInterface {

    public static AppPreference appPreference;
    public static String c_date;
/* TODO */
    FrameLayout container_layout;

    public static API serviceApi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragments_activity);

        container_layout = findViewById(R.id.fragment_container);
        appPreference = new AppPreference(this);

        Intent getNewCard = getIntent();
        //getNewCard.getStringExtra("newCard");


        //Log.e("created_at: ", c_date);

       serviceApi = ApiClient.getApiClient().create(API.class);

        if (container_layout != null){
            if (savedInstanceState != null){
                return;
            }

            //check login status from sharedPreference
            if (appPreference.getLoginStatus()){
                //when true
                getSupportFragmentManager()
                        .beginTransaction()
                        .add(R.id.fragment_container, new ProfileFragment())
                        .commit();
            } else {
                // when get false
                getSupportFragmentManager()
                        .beginTransaction()
                        .add(R.id.fragment_container, new LoginFragment())
                        .commit();
            }
        }

    } // ending onCreate


    // overridden from MyInterface
    @Override
    public void register() {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_container, new RegistrationFragment())
                .addToBackStack(null)
                .commit();
    }
    @Override
    public void login(String name,String email, String t) {

        appPreference.setDisplayEmail(email);


        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_container, new ProfileFragment())
                .commit();
    }
    @Override
    public void logout() {
        appPreference.setLoginStatus(false);
        appPreference.setDisplayName("Name");
        appPreference.setDisplayEmail("Email");


        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_container, new LoginFragment())
                .commit();
    }
}
