package com.seas.cinesaragon_seas.Controller.Fragments;


import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.seas.cinesaragon_seas.Model.User;
import com.seas.cinesaragon_seas.R;
import com.seas.cinesaragon_seas.Model.Remote.SesionInterface;
import com.seas.cinesaragon_seas.Controller.Extras.Container;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 */
public class LoginFragment extends Fragment {

    private SesionInterface loginFromActivityListener;
    private TextView registerTV;

    private EditText emailInput, passwordInput;
    private Button loginBtn;

    public LoginFragment() {
        // Required empty public constructor
    }
//// TODO: 11/08/2019


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_login, container, false);

        // for login
        emailInput = view.findViewById(R.id.emailInput);
        passwordInput = view.findViewById(R.id.passwordInput);
        loginBtn = view.findViewById(R.id.loginBtn);
        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loginUser();
            }
        });

        registerTV = view.findViewById(R.id.registerTV);
        registerTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loginFromActivityListener.register();
            }
        });
        return view;
    } //ending onCreateView

    private void loginUser() {
        String Email = emailInput.getText().toString();
        String Password = passwordInput.getText().toString();

        if (TextUtils.isEmpty(Email)) {
            Container.appPreference.showToast("Your email is required.");
        } else if (!Patterns.EMAIL_ADDRESS.matcher(Email).matches()) {
            Container.appPreference.showToast("Invalid email");
        } else if (TextUtils.isEmpty(Password)) {
            Container.appPreference.showToast("Password required");

        } else {
            Call<User> userCall = Container.serviceApi.login(Email, Password);
            userCall.enqueue(new Callback<User>() {
                @Override
                public void onResponse(Call<User> call, Response<User> response) {
                    if (response.body().getResponse().equals("data")) {
                        Container.appPreference.setLoginStatus(true); // set login status in sharedPreference
                        loginFromActivityListener.login(
                                response.body().getName(),
                                response.body().getEmail(),
                                response.body().getNumero());
                               // response.body().getCaducidad(),
                              //  response.body().getCVV());

                        Container.appPreference.showToast("Bienvenido");

                        Container.appPreference.setDisplayName( response.body().getName());
                        Container.appPreference.setDisplayEmail( response.body().getEmail());
                       // Container.appPreference.setDisplayCard( response.body().getNumero());
                      //  Container.appPreference.setDisplayf( response.body().getCaducidad());
                      //  Container.appPreference.setDisplayccv( response.body().getCVV());



                    } else if (response.body().getResponse().equals("login_failed")) {
                        Container.appPreference.showToast("El Usuario no existe");
                        emailInput.setText("");
                        passwordInput.setText("");
                    }
                }

                @Override
                public void onFailure(Call<User> call, Throwable t) {
                    Container.appPreference.showToast("Error. No conecta con el PHP");
                }


            });
        }
        //en}ding loginUser()
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Activity activity = (Activity) context;
        loginFromActivityListener = (SesionInterface) activity;
    }

}
