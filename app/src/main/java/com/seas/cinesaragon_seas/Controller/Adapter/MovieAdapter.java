package com.seas.cinesaragon_seas.Controller.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import java.util.ArrayList;
import java.util.List;

import com.seas.cinesaragon_seas.Controller.Extras.MovieFilter;
import com.seas.cinesaragon_seas.Model.Movies;
import com.seas.cinesaragon_seas.Model.User;
import com.seas.cinesaragon_seas.R;



public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.MyViewHolder> implements Filterable {

    public List<Movies> movies;
    public List<User> users;
    List<Movies> moviesFilter;
    private Context context;
    private RecyclerViewClickListener mListener;
    MovieFilter filter;

    public MovieAdapter(List<Movies> movies, Context context, RecyclerViewClickListener listener) {
        this.movies = movies;
        this.moviesFilter = movies;
        this.context = context;
        this.mListener = listener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.movie_list, parent, false);
        return new MyViewHolder(view, mListener);
    }

    @SuppressLint("CheckResult")
    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {

        holder.mName.setText(movies.get(position).getNombre());
       holder.mDescripcion.setText(movies.get(position).getDescripcion());


        RequestOptions requestOptions = new RequestOptions();
        requestOptions.skipMemoryCache(true);
        requestOptions.diskCacheStrategy(DiskCacheStrategy.NONE);
        requestOptions.placeholder(R.drawable.camera);
        requestOptions.error(R.drawable.camera);

        Glide.with(context)
                .load(movies.get(position).getPicture())
                .apply(requestOptions)
                .into(holder.mPicture);



    }

    @Override
    public int getItemCount() {
        return movies.size();
    }

    @Override
    public Filter getFilter() {
        if (filter==null) {
            filter=new MovieFilter((ArrayList<Movies>) moviesFilter,this);

        }
        return filter;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private RecyclerViewClickListener mListener;
        private ImageView mPicture;

        private TextView mName, mDescripcion;
        private RelativeLayout mRowContainer;

        public MyViewHolder(View itemView, RecyclerViewClickListener listener) {
            super(itemView);
            mPicture = itemView.findViewById(R.id.poster);
            mName = itemView.findViewById(R.id.title);
            mDescripcion = itemView.findViewById(R.id.descripcion);
            mRowContainer = itemView.findViewById(R.id.row_container);
            mListener = listener;
            mRowContainer.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.row_container:
                    mListener.onRowClick(mRowContainer, getAdapterPosition());
                    break;
                default:
                    break;
            }
        }
    }

    public interface RecyclerViewClickListener {
        void onRowClick(View view, int position);

    }
}
