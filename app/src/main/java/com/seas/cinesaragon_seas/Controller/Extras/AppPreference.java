package com.seas.cinesaragon_seas.Controller.Extras;

import android.content.Context;
import android.content.SharedPreferences;
import android.widget.Toast;

import com.seas.cinesaragon_seas.R;


// Shared Preference METHODS

public class AppPreference {
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private Context context;

    public AppPreference(Context context){
        this.context = context;
        sharedPreferences = context.getSharedPreferences(String.valueOf(R.string.s_pref_file), Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();

    }

    //Setting login status
    public void setLoginStatus(boolean status){
        editor.putBoolean(String.valueOf(R.string.s_pref_login_status), status);
        editor.commit();
    }
    public boolean getLoginStatus(){
        return sharedPreferences.getBoolean(String.valueOf(R.string.s_pref_login_status), false);
    }

    // For Name
    public void setDisplayName(String name){
        editor.putString(String.valueOf(R.string.s_pref_name), name);
        editor.commit();
    }
    public String getDisplayName(){
        return sharedPreferences.getString(String.valueOf(R.string.s_pref_name), "name");
    }

    //For email
    public void setDisplayEmail(String email){
        editor.putString(String.valueOf(R.string.s_pref_mail), email);
        editor.commit();
    }
    public String getDisplayEmail(){
        return sharedPreferences.getString(String.valueOf(R.string.s_pref_mail), "email");
    }

  //For tarjeta
    public void setDisplayCard(String card){
        editor.putString(String.valueOf(R.string.s_pref_card), card);
        editor.commit();
    }
    public String getDisplayCard(){
        return sharedPreferences.getString(String.valueOf(R.string.s_pref_card), "tarjeta");
    }
     /*
    //For email
    public void setDisplayf(String f){
        editor.putString(String.valueOf(R.string.s_pref_email), f);
        editor.commit();
    }
    public String getDisplayf(){
        return sharedPreferences.getString(String.valueOf(R.string.s_pref_email), "");
    }
    //For email
    public void setDisplayccv(String ccv){
        editor.putString(String.valueOf(R.string.s_pref_email), ccv);
        editor.commit();
    }
    public String getDisplayccv(){
        return sharedPreferences.getString(String.valueOf(R.string.s_pref_email), "");
    }*/

    // For TOAST Message for response
    public void showToast(String message){
        Toast.makeText(context, message, Toast.LENGTH_LONG).show();
    }

}