package com.seas.cinesaragon_seas.Controller.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.seas.cinesaragon_seas.Controller.Extras.CinemaFilter;
import com.seas.cinesaragon_seas.Model.Cinemas;
import com.seas.cinesaragon_seas.R;

import java.util.ArrayList;
import java.util.List;

public class CinemaAdapeter  extends RecyclerView.Adapter<CinemaAdapeter.MyViewHolder> implements Filterable{


        public List<Cinemas> cinemas;
        List<Cinemas> cinemasFilter;
        private Context context;
        private RecyclerViewClickListener mListener;
        CinemaFilter filter;

        public CinemaAdapeter(List<Cinemas> cinemas, Context context, RecyclerViewClickListener listener) {
            this.cinemas = cinemas;
            this.cinemasFilter = cinemas;
            this.context = context;
            this.mListener = listener;
            /* TODO */
        }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cinema_list, parent, false);
        return new MyViewHolder(view, mListener);
    }

        @SuppressLint("CheckResult")
        @Override
        public void onBindViewHolder(final MyViewHolder holder, int position) {

            holder.cinemaName.setText(cinemas.get(position).getNombre());
            holder.desCr.setText(cinemas.get(position).getDescripcion());
            holder.info.setText(cinemas.get(position).getDireccion());



            RequestOptions requestOptions = new RequestOptions();
            requestOptions.skipMemoryCache(true);
            requestOptions.diskCacheStrategy(DiskCacheStrategy.NONE);
            requestOptions.placeholder(R.drawable.camera);
            requestOptions.error(R.drawable.camera);

            Glide.with(context)
                    .load(cinemas.get(position).getFoto1())
                    .apply(requestOptions)
                    .into(holder.mPicture);


        }

        @Override
        public int getItemCount() {
            return cinemas.size();
        }

        @Override
        public Filter getFilter() {
            if (filter==null) {
                filter=new CinemaFilter((ArrayList<Cinemas>) cinemasFilter,this);

            }
            return filter;
        }

        public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

            private RecyclerViewClickListener mListener;
            private ImageView mPicture;
            private TextView cinemaName, desCr, info;
            private RelativeLayout mRowContainer2;

            public MyViewHolder(View itemView, RecyclerViewClickListener listener) {
                super(itemView);
                mPicture = itemView.findViewById(R.id.cinema_image);
                cinemaName = itemView.findViewById(R.id.cine_name_list);
                desCr= itemView.findViewById(R.id.cine_desc_list);
                info = itemView.findViewById(R.id.cine_contact_list);
                mRowContainer2 = itemView.findViewById(R.id.row_container_cinema);
                mListener = listener;
                mRowContainer2.setOnClickListener(this);
            }

            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.row_container_cinema:
                        mListener.onRowClick(mRowContainer2, getAdapterPosition());
                        break;
                    default:
                        break;
                }
            }
        }

        public interface RecyclerViewClickListener {
            void onRowClick(View view, int position);

        }
}
