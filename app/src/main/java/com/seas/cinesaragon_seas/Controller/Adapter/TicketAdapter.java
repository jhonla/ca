package com.seas.cinesaragon_seas.Controller.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.seas.cinesaragon_seas.Controller.Extras.TicketFilter;
import com.seas.cinesaragon_seas.Model.User;
import com.seas.cinesaragon_seas.R;

import java.util.ArrayList;
import java.util.List;


public class TicketAdapter extends RecyclerView.Adapter<TicketAdapter.MyViewHolder> implements Filterable {

    public List<com.seas.cinesaragon_seas.Model.Ticket> tickets;
    public List<User> users;
    List<com.seas.cinesaragon_seas.Model.Ticket> ticketsFilter;
    private Context context;
    private RecyclerViewClickListener mListener;
    TicketFilter filter;

    public TicketAdapter(List<com.seas.cinesaragon_seas.Model.Ticket> tickets, Context context, RecyclerViewClickListener listener) {
        this.tickets = tickets;
        this.ticketsFilter = tickets;
        this.context = context;
        this.mListener = listener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.ticket_list, parent, false);
        return new MyViewHolder(view, mListener);
    }

    @SuppressLint("CheckResult")
    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {

        ///colocamos en el row container la lista

        holder.mName.setText(tickets.get(position).getPelicula());
        holder.mCinema.setText(tickets.get(position).getCine());
        holder.mSesion.setText(tickets.get(position).getSesion());
        holder.mDate.setText(tickets.get(position).getCine());

    }

    @Override
    public int getItemCount() {
        return tickets.size();
    }

    @Override
    public Filter getFilter() {
        if (filter == null) {
            filter = new TicketFilter((ArrayList<com.seas.cinesaragon_seas.Model.Ticket>) ticketsFilter, this);

        }
        return filter;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private RecyclerViewClickListener mListener;
        private TextView mName, mCinema, mSesion, mDate;
        private RelativeLayout mRowContainer;

        public MyViewHolder(View itemView, RecyclerViewClickListener listener) {
            super(itemView);
            mName = itemView.findViewById(R.id.title);
            mCinema = itemView.findViewById(R.id.cinemat);
            mSesion = itemView.findViewById(R.id.sesiont);
            mDate = itemView.findViewById(R.id.date);
            mRowContainer = itemView.findViewById(R.id.row_container);
            mListener = listener;
            mRowContainer.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.row_container:
                    mListener.onRowClick(mRowContainer, getAdapterPosition());
                    break;
                default:
                    break;
            }
        }
    }

    public interface RecyclerViewClickListener {
        void onRowClick(View view, int position);

    }
}
